import * as React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native'
import WebRuntime, { call } from '../index'

export default class App extends React.Component {
  public run() {
    Promise
      .all([
        call("generateMnemonic"),
        // call("getLatestBlock").then((d: any) => d.gasUsed),
        // call('randomBytesHex'),
        // call('encryptV3Wallet', { password: '1234' }),
      ])
      .then(dat => Alert.alert('INFO', dat.join('\n')))
      .catch(d => Alert.alert('INFO', `ERR:\n${d.message}`))
  }

  public render() {
    return <View style={styles.main}>
      <WebRuntime />
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <TouchableOpacity onPress={() => this.run()}>
          <Text style={{ fontSize: 20 }}>TAP HERE TO RUN STUFF</Text>
        </TouchableOpacity>
      </View>
    </View>
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
