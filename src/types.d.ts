///////////////////////////////////////////////////////////////////////////////
// EXTERNAL SCOPE
///////////////////////////////////////////////////////////////////////////////

export type RequestActionKeys = "generateMnemonic"
  // | "getLatestBlock"
  | "getAllEntities"
  | "getEntityMetadata"
  | "getEntityOpenProcesses"
  | "getProcessMetadata"
  | "getCensusProof"
  | "mnemonicToAddress"

export interface WebRuntimePayload {
  id: number;
  type: string;
  name: (string | null);
  data: any;
  error: (string | null);
}

export interface RequestQueueEntry {
  action: RequestActionKeys;
  dispatched: boolean;
  id: number;
  data: any;
  resolve: (value: any) => void;
  reject: (reason: Error) => void;
  timeout: any;
}

///////////////////////////////////////////////////////////////////////////////
// INTERNAL SCOPE
///////////////////////////////////////////////////////////////////////////////

export interface WebRuntimeMessage {
  action: RequestActionKeys,
  id: number,
  data: any
}

export type HandlersMap = { [K in RequestActionKeys]?: ((msg: WebRuntimeMessage) => void) };

///////////////////////////////////////////////////////////////////////////////
// HANDLER'S SCOPE
///////////////////////////////////////////////////////////////////////////////

export interface Entity {
  name: string
  address: string
  censusRequestUrl: string
}

export interface Process {
  name: string
  startBlock: number
  endBlock: number
  question: string
  votingOptions: string[]
  voteEncryptionPublicKey: string
}
