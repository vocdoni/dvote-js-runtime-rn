import 'babel-polyfill'
import { WebRuntimeMessage, HandlersMap } from "./types.d"
import {
  VOTING_ENTITY_CONTRACT_ADDRESS,
  BLOCKCHAIN_PROVIDER_URL,
  VOTING_PROCESS_CONTRACT_ADDRESS
} from "./runtime-constants"
// import Web3 from "web3"
import bip39 from "bip39"
import { Wallet } from 'ethers'

// TODO: INVESTIGATE WHY importing web3 produces 1Mb
//       but importing dvote-client doubles the size
import { Entity, Process, Census } from "dvote-client"

// import hdKey from "ethereumjs-wallet/hdKey"

import { init, replyMessage, replyError, emitEvent } from "./runtime-messaging"

// let web3 = new Web3(new Web3.providers.HttpProvider(BLOCKCHAIN_PROVIDER_URL))

///////////////////////////////////////////////////////////////////////////////
// SETUP
///////////////////////////////////////////////////////////////////////////////

const handlers: HandlersMap = {
  generateMnemonic,
  // getLatestBlock,
  getAllEntities,
  getEntityMetadata,
  getEntityOpenProcesses,
  getProcessMetadata,
  getCensusProof,
  mnemonicToAddress

  // randomBytesHex,
  // encryptV3Wallet
}

init(handlers)

///////////////////////////////////////////////////////////////////////////////
// LIFECYCLE
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// IMPLEMENTATIONS HERE
///////////////////////////////////////////////////////////////////////////////

function generateMnemonic(msg: WebRuntimeMessage) {
  const mnemonic = bip39.generateMnemonic(192)

  replyMessage(msg.id, mnemonic)
}

function mnemonicToAddress(msg: WebRuntimeMessage) {
  try {
    const payload = msg.data
    if (!payload) throw new Error();
    const wallet = Wallet.fromMnemonic(payload.mnemonic, payload.path || "m/44'/60'/0'/0/0")

    replyMessage(msg.id, wallet.address)
  }
  catch (err) {
    replyError(msg.id, "Unable to access the wallet data")
  }
}

function getAllEntities(msg: WebRuntimeMessage) {
  const entityInstance = new Entity(BLOCKCHAIN_PROVIDER_URL, VOTING_ENTITY_CONTRACT_ADDRESS)

  entityInstance.getAll().then((entities) => {
    replyMessage(msg.id, entities)
  }).catch(err => replyError(msg.id, err.message))
}

function getEntityMetadata(msg: WebRuntimeMessage) {
  const entityInstance = new Entity(BLOCKCHAIN_PROVIDER_URL, VOTING_ENTITY_CONTRACT_ADDRESS)

  const entityAddress: string = msg.data
  if (!entityAddress) return replyError(msg.id, "Invalid address")

  entityInstance.get(entityAddress).then(metadata => {
    replyMessage(msg.id, metadata)
  }).catch(err => replyError(msg.id, err.message))
}

function getEntityOpenProcesses(msg: WebRuntimeMessage) {
  const processInstance = new Process(BLOCKCHAIN_PROVIDER_URL, VOTING_PROCESS_CONTRACT_ADDRESS)

  const entityAddress: string = msg.data
  if (!entityAddress) return replyError(msg.id, "Invalid address")

  processInstance.getProcessesIdsByOrganizer(entityAddress)
    .then((processIds: string[]) => {
      return Promise.all(processIds.map(id => processInstance.getMetadata(id)))
    })
    .then((metadata: object[]) => {
      replyMessage(msg.id, metadata)
    })
    .catch(err => replyError(msg.id, err.message))
}

function getProcessMetadata(msg: WebRuntimeMessage) {
  const processInstance = new Process(BLOCKCHAIN_PROVIDER_URL, VOTING_PROCESS_CONTRACT_ADDRESS)

  const processId: string = msg.data
  if (!processId) return replyError(msg.id, "Invalid process id")

  processInstance.getMetadata(processId).then((metadata: any) => {
    replyMessage(msg.id, metadata)
  }).catch(err => replyError(msg.id, err.message))
}

function getCensusProof(msg: WebRuntimeMessage) {
  // User address
  const address: string = msg.data.address
  if (typeof address != "string" || !address.match(/^0x[0-9a-fA-F]{40}$/)) {
    return replyError(msg.id, "Invalid address")
  }

  const processId: string = msg.data.processId
  if (!processId) return replyError(msg.id, "Invalid process ID")

  const censusId: string = msg.data.censusId
  if (!censusId) return replyError(msg.id, "Invalid census ID")

  // Get the census URL from the blockchain
  const census = new Census()
  census.initBlockchain(BLOCKCHAIN_PROVIDER_URL, VOTING_PROCESS_CONTRACT_ADDRESS)
  census.getMetadata(processId).then(censusMetadata => {

    const censusProofUrl = censusMetadata.censusProofUrl

    return Census.getProof(address, censusId, censusProofUrl).then(proof => {
      if (!proof) throw new Error("Invalid proof")
      replyMessage(msg.id, proof.raw)
    })
  }).catch(err => replyError(msg.id, err.message))
}

// function randomBytesHex(msg) {
// 	const data = nacl.to_hex(nacl.random_bytes(32))

// 	replyMessage(msg.id, data)
// }

// function encryptV3Wallet(msg) {
// 	if (!msg.data || !msg.data.password || typeof msg.data.password != "string") {
// 		return replyError(msg.id, "Invalid password provided")
// 	}

// 	let mnemonic
// 	const path = "m/44'/60'/0'/0/0"

// 	try {
// 		if (msg.data.mnemonic) mnemonic = msg.data.mnemonic
// 		else mnemonic = bip39.generateMnemonic(192)

// 		const seed = bip39.mnemonicToSeed(mnemonic)

// 		const rootWallet = hdKey.fromMasterSeed(seed)
// 		const wallet = rootWallet.derivePath(path).getWallet()

// 		// wallet.getAddressString()
// 		// wallet.getPublicKeyString()
// 		// wallet.getPrivateKeyString()

// 		replyMessage(msg.id, wallet.toV3(msg.data.password))
// 	} catch (err) {
// 		replyError(msg.id, err.message)
// 	}
// }

// function toWei(msg) {
// 	const result = web3.utils.toWei(msg.data.amount, msg.data.unit)
// 	replyMessage(msg.id, result)
// }

///////////////////////////////////////////////////////////////////////////////
// HELPERS
///////////////////////////////////////////////////////////////////////////////

// let needsReconnect = false

// function checkWeb3Connection() {
//   return new Promise(resolve => {
//     if (needsReconnect) {
//       // emitEvent("disconnected")
//       web3.setProvider(BLOCKCHAIN_PROVIDER_URL)
//       needsReconnect = false
//       setTimeout(resolve, 10)
//     }
//     else {
//       resolve()
//     }
//   })
// }

///////////////////////////////////////////////////////////////////////////////
// IPFS BOILERPLATE
///////////////////////////////////////////////////////////////////////////////

// import Ipfs from "ipfs"

// interface IpfsNodeInfo {
//   id: string,
//   addresses: string[],
//   publicKey: string
// }
// interface IpfsMessage {
//   data: Buffer,
//   from: string
// }
// let nodeInfo: IpfsNodeInfo
// const options = {
//   EXPERIMENTAL: {
//     pubsub: true
//   },
//   repo: 'ipfs-repo',
//   config: {
//     Addresses: {
//       Swarm: [
//         '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star',
//         '/dns4/ws-star1.par.dwebops.pub/tcp/443/wss/p2p-websocket-star'
//       ]
//     },
//     Bootstrap: [
//       "/ip4/51.15.89.137/tcp/4001/ipfs/QmQYCRedFxW8B1KgPSEFCfudaJSnZYwLGA1wVxmhZ711Hf",
//       "/ip4/84.88.85.1/tcp/4001/ipfs/QmecBnn7CxgCDuWHZsgULr23f4vEHRAM2Qmwt1PofT3DxA"
//       // 	"/ip4/51.15.99.99/tcp/4001/ipfs/QmWHnrypf9HFWemBZp6T5EQvqB3MBvPNDdBLBUPpGsh1Lq",
//       // 	"/ip4/51.15.233.136/tcp/4001/ipfs/QmRNJiBnnBy4rH2wfYQ2AFe3vwD5yyMd5PjtK2bgrMzS9f"
//     ]
//   }
// }

// const ROOM_NAME = "vocdoni_pubsub_testing"
// const node = new Ipfs(options)
// Buffer = node.types.Buffer

// node.once('start', () => {
//   node.id()
//     .then((info: IpfsNodeInfo) => {
//       nodeInfo = info
//       console.log('Node is ready.', nodeInfo)

//       // HANDLE INCOMING MESSAGES
//       node.pubsub.subscribe(ROOM_NAME, pubsubMessageHandler)

//       // BROADCAST
//       setInterval(() => {
//         node.pubsub.publish(ROOM_NAME, Buffer.from("I AM JORDI > APP"))
//       }, 5000)
//     })
//     .catch((error: Error) => console.log(error))
// })

// function pubsubMessageHandler(message: IpfsMessage) {
//   const myNode = nodeInfo.id
//   const hash = message.data.toString()
//   const messageSender = message.from

//   console.log("GOT", messageSender, hash, myNode !== messageSender ? "" : "(me)")
// }
