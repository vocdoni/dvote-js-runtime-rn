//////////////////////////////////////////
// POA nou.network
//////////////////////////////////////////

// // BLOCKCHAIN
// export const BLOCKCHAIN_PROVIDER_URL = "http://web3.testnet.vocdoni.io:8545"
// export const BLOCKCHAIN_NETWORK_ID = "34382"

// // CONTRACTS
// export const VOTING_ENTITY_CONTRACT_ADDRESS = "0xA0dcA4b8Fd2cE8f75f4ba7bDeF5B91BFA6948460"
// export const VOTING_PROCESS_CONTRACT_ADDRESS = "0x1a15631064a113Cc8bb7C97e78939FbeCc6b596d"

//////////////////////////////////////////
// POA vocdoni.io
//////////////////////////////////////////

// BLOCKCHAIN
export const BLOCKCHAIN_PROVIDER_URL = "http://node.testnet.vocdoni.io:8545"
export const BLOCKCHAIN_NETWORK_ID = "1714"

// CONTRACTS
export const VOTING_ENTITY_CONTRACT_ADDRESS="0x60d28ccb4946E9a65b883F4cb693e61593cB5dA2"
export const VOTING_PROCESS_CONTRACT_ADDRESS="0x26FBD439B4AF6510B98c4c2C8F1652bA25b9Ef4f"
