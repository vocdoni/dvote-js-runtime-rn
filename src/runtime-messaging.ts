import { WebRuntimeMessage } from "./types";

let handlerMap = {}

///////////////////////////////////////////////////////////////////////////////
// INIT
///////////////////////////////////////////////////////////////////////////////

export function init(handlers: object) {
  // Register message handlers
  handlerMap = handlers

  // Loaded
  document.addEventListener('DOMContentLoaded', () => {
    // Listen for incoming messages
    document.addEventListener("message", (ev: WebRuntimeMessage) => gotMessage(ev))
  });
}

///////////////////////////////////////////////////////////////////////////////
// INCOMING MESSAGES
///////////////////////////////////////////////////////////////////////////////

function gotMessage(event: { data: string }) {
  let msg: (WebRuntimeMessage | null) = null
  try {
    msg = <WebRuntimeMessage>JSON.parse(event.data)
    const handler = handlerMap[msg.action]

    if (typeof handler !== "function") return replyError(msg.id, "Unknown method " + msg.action)
    else handler(msg)
  }
  catch (err) {
    if (msg) replyError(msg.id, err.message)
  }
}

///////////////////////////////////////////////////////////////////////////////
// OUTGOING MESSAGES
///////////////////////////////////////////////////////////////////////////////

export function replyMessage(id: number, data?: any) {
  window.postMessage(JSON.stringify({ id: id || 0, data: data || {} }))
}

export function replyError(id: number, text?: string) {
  window.postMessage(JSON.stringify({ id: id || 0, error: text || "" }))
}

export function emitEvent(name: string) {
  window.postMessage(JSON.stringify({ type: "event", name }))
}
