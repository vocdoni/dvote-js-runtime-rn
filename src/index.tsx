/**
 * NOTE:
 * The purpose of this component is to overcome the limitations of the Javascript
 * environment available to apps running on React Native/Expo.
 *
 * At the time of writing, modules like window.crypto or require('crypto') are
 * available to the major browsers, as well as NodeJS. However, RN Javascript does not
 * provide such API so NPM packages depending on it will not work.
 *
 * The component provides access to a full fledged JS runtime, that will only run
 * previously bundled static HTML/JS code.
 *
 * This is intended as a workaround until RN/Expo provides full support
 * for cryptographic and blockchain libraries.
 */

import * as React from 'react'
import { WebView, View, NativeSyntheticEvent, WebViewMessageEventData } from 'react-native'
import { RequestActionKeys, RequestQueueEntry, WebRuntimePayload } from "./types.d"

// alternate approach (bundling assets)
// info: https://medium.com/@filipedegrazia/embedding-a-local-website-on-your-expo-react-native-project-eea322738872

import { html } from "./dist/index.json"

let webviewInstance: (WebView | null) = null
let webviewReady = false
let requestCounter = 0
const requestQueue: RequestQueueEntry[] = []

export default class WebRuntime extends React.Component {
  public componentWillUnmount() {
    webviewReady = false
    webviewInstance = null
  }

  public render() {
    return (
      <View style={{ display: 'none', position: 'absolute', flex: 0, height: 0, width: 0 }}>
        <WebView
          useWebKit={true}
          source={{ html, baseUrl: "file:///" }}
          ref={(ref) => { webviewInstance = ref }}
          originWhitelist={['*']}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          onError={err => { if (__DEV__) console.error(err) }}
          onLoadEnd={() => { webviewReady = true }}
          onMessage={event => gotMessage(event)} />
      </View>
    )
  }
}

/**
 * Invoke a function to be run within the WebRuntime
 * @param key string identifying the function to call
 * @param data arbitrary (optional) data to be passed to the function
 */
export function call(key: RequestActionKeys, data: object | string | null = null): Promise<any> {
  if (!key) return Promise.reject(new Error('Please, provide a valid key to call'))
  // if (!webviewInstance) return Promise.reject(new Error('The runtime is not loaded yet'))
  // if (!webviewReady) return Promise.reject(new Error('The runtime is not ready yet'))

  return new Promise((resolve, reject) => {
    requestCounter++

    const newRequest: RequestQueueEntry = {
      action: key,
      dispatched: false,
      id: requestCounter,
      data,
      resolve,
      reject,
      timeout: setTimeout(() => expireRequest(requestCounter), 20 * 1000),
    }
    requestQueue.push(newRequest)

    if (webviewInstance && webviewReady) { // dispatch now
      dispatchMessage({
        action: newRequest.action,
        id: newRequest.id,
        data: newRequest.data
      })
    }
    // else => wait for the queue to process it
  })
}

// INTERNAL

/**
 * Received an event from the WebView environment (mainly a response)
 * @param event
 */
function gotMessage(event: NativeSyntheticEvent<WebViewMessageEventData>) {
  let msgPayload: WebRuntimePayload
  let idx
  try {
    msgPayload = JSON.parse(event.nativeEvent.data)

    // Internal event
    if (msgPayload.type === 'event') {
      if (msgPayload.name === 'disconnected') {
        // setTimeout(() => webviewInstance.reload(), 1500)
      }
      return
    }

    // Message response
    idx = requestQueue.findIndex(r => r.id === msgPayload.id)
    if (idx < 0) return
    if (msgPayload.error) {
      if (msgPayload.error === 'connection not open') {
        if (webviewInstance) webviewInstance.reload()
      }

      if (typeof requestQueue[idx].reject === 'function') {
        requestQueue[idx].reject(new Error(msgPayload.error))
        clearTimeout(requestQueue[idx].timeout)
      }
    }
    else if (typeof requestQueue[idx].resolve === 'function') {
      requestQueue[idx].resolve(msgPayload.data)
      clearTimeout(requestQueue[idx].timeout)
    }
    // clean
    delete requestQueue[idx].resolve
    delete requestQueue[idx].reject
    requestQueue.splice(idx, 1)
  }
  catch (err) {
    if (typeof idx === 'number' && requestQueue[idx].reject) {
      clearTimeout(requestQueue[idx].timeout)
      return requestQueue[idx].reject(err)
    }
    if (__DEV__) console.error(err)
  }
}

function expireRequest(id: number) {
  const idx = requestQueue.findIndex(r => r.id === id)
  if (idx < 0) return
  requestQueue[idx].reject(new Error('Webview timeout'))

  delete requestQueue[idx].resolve
  delete requestQueue[idx].reject
  requestQueue.splice(idx, 1)
}

interface RuntimeMessage {
  action: RequestActionKeys
  id: number,
  data: object | string | null
}
function dispatchMessage(message: RuntimeMessage) {
  if (!webviewInstance) return

  let idx = requestQueue.findIndex(r => r.id == message.id)
  if (idx < 0) return
  else if (requestQueue[idx].dispatched) return

  requestQueue[idx].dispatched = true
  webviewInstance.postMessage(JSON.stringify(message))
}

/**
 * Dispatches the first pending message
 */
function dispatchPendingMessages() {
  if (!webviewReady || !webviewInstance) return

  const reqs = requestQueue.filter(r => !r.dispatched)
  if (reqs.length == 0) return

  dispatchMessage({
    action: reqs[0].action,
    id: reqs[0].id,
    data: reqs[0].data
  })
}
setInterval(dispatchPendingMessages, 400)
