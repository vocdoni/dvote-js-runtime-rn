# WEB RUNTIME

The purpose of this component is to overcome the limitations of the Javascript environment available to apps running on React Native/Expo.

At the time of writing, modules like window.crypto or require('crypto') are available to the major browsers, as well as NodeJS. However, RN Javascript does not provide such API so NPM packages depending on it will not work.

The component provides access to a full fledged JS runtime, that will only run previously bundled static HTML/JS code.

This is intended as a workaround until RN/Expo provides full support for cryptographic and blockchain libraries.

## Usage

See `example/example-usage.tsx` for a reference implementation.

* Render the WebRuntime component
* Make a `call`

```tsx
import WebRuntime, { call } from '.../web-runtime'

export default class App extends Component {
  private someEvent() {
    call("generateMnemonic", { param: null }).then(result => {
      console.log("RESULT", result)
    }).catch(err => {
      console.error(err)
    })
  }
  public render() {
    return <View>
      <WebRuntime />
      { /* your views and content */ }
    </View>
  }
  // ...
}
```

The `WebRuntime` hook and the `call` invokations can live in separate components. Ideally, the `WebRuntime` would be placed at the root of your main component, so it is always loaded.


## Emitted errors:
- `Please, provide a valid key to call`
  - The first parameter passed to `call(...)` is not valid
  - Refer to `index.tsx` > `ActionKeys` type
- `The runtime is not loaded yet`
  - You need to allow a bit more of time, so that the internal WebView gets loaded
- `The runtime is not ready yet`
  - You need to allow a bit more of time, so that the internal WebView gets loaded
